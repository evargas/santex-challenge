export const USDollar = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
});

export const truncateStr = (str, number = 200) => (number <= str.length) ? str.slice(0, number) + ' ...' : str