import { gql } from '@apollo/client';

export const GET_LOCATIONS = gql`
  {
    products {
      items {
        id
        name
        description
        variantList {
          items {
            price
            name
            id
            featuredAsset {
              id
              preview
            }
            assets {
              id
              preview
            }
            
          }
        }
      }
    }
  }
`;
