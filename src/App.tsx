import { Header } from './components/Header';
import { ProductList } from './components/ProductList';
import styled from 'styled-components';

const CustomContainer = styled.div`
  width: 80vw;
  margin: 0 10vw;
`

function App() {
  return (
    <CustomContainer>
      <Header />
      <ProductList />
    </CustomContainer>
  );
}

export default App;
