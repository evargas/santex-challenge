export const get_mock_image = () => {
    const number = Math.floor(Math.random() * 30)
    return number ? 
        `https://picsum.photos/id/${number}/500/333` : 
        'https://dkofva0t6jnyn.cloudfront.net/sites/default/files/styles/amp_blog_image_large/public/consumer/blog/7-lecciones-yoda-1138x658.jpg'
}