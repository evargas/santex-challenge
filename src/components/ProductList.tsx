import { useQuery } from '@apollo/client';
import styled from 'styled-components'
import { GET_LOCATIONS } from '../graphql/queries';
import { get_mock_image } from '../mock/img_mock';
import { truncateStr, USDollar } from '../utils/utils';

const CustomContainer = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: space-between;
  row-gap: 20px;
  column-gap: 10px;
  flex-wrap: wrap;

  .product-item{
    flex: 1;
    min-width: 25vw;
    max-width: 25vw;
    height: 20vw;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    position: relative;
    overflow: hidden;
    transition: ease-in-out all 0.5s;

    &:hover{
      filter: sepia(80%);
      background-position: 33% center;
      .product-desc{
        opacity: 1;
        bottom: 21px;
      }
      .product-name,
      .product-price{
        background: rgba(255, 255, 255, 0.7);
        overflow: inherit;
        white-space: inherit;
        text-overflow: inherit;
      }
    }

    .product-name{
      position: absolute;
      margin: 0;
      top: 0px;
      left: 0px;
      right: 0px;
      padding: 5px;
      background: rgba(255, 255, 255, 0.3);
      transition: ease-in-out all 0.5s;
      overflow: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
    }

    .product-price{
      position: absolute;
      margin: 0;
      bottom: 0px;
      left: 0px;
      right: 0px;
      padding: 5px;
      background: rgba(255, 255, 255, 0.3);
      text-align: right;
    }

    .product-desc{
      position: absolute;
      bottom: -200px;
      opacity: 0;
      left: 2px;
      right: 2px;
      padding: 10px;
      background: #fff;
      text-align: justify;
      transition: ease-in-out all 0.5s;
    }

    button{
      border: solid 2px #274b7d;
      background-color: #fff;
      padding: 8px;
      text-align: center;
      min-width: 50px;
      font-weight: bolder;
      text-transform: uppercase;
      position: absolute;
      bottom: 5px;
      left: 5px;
      cursor: pointer;
      transition: ease-in-out all 0.5s;

      &:hover{
        border: solid 2px white;
        background-color: #274b7d;
        color: #fff;  
      }
    }
  }

`

export function ProductList() {

  const handleClick = () => {}

  const { loading, error, data } = useQuery(GET_LOCATIONS);
  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error : {error.message}</p>;

  let productsWithVariants: Array<any> = []

  data.products.items.forEach((item: any) => {
    const variants = item.variantList.items.map((variant: any) => ({
      name: variant.name,
      id: variant.id,
      price: USDollar.format(variant.price),
      description: truncateStr(item.description),
      image: get_mock_image()
    }))

    productsWithVariants = [...productsWithVariants, ...variants]
  })

  return (
    <CustomContainer>
      {
        productsWithVariants.map((product: any) =>
          <div className='product-item' key={product.id} style={{ backgroundImage: `url(${product.image})` }}>
            <h2 className='product-name'>{product.name}</h2>
            <p className='product-desc'>{product.description}</p>
            <h3 className='product-price'>{product.price}</h3>
            <button onClick={() => handleClick()}>Add</button>
          </div>
        )
      }
    </CustomContainer>
  )
}
