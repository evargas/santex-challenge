import styled from 'styled-components'

const CustomHeader = styled.header`
  height: 100px;
  padding: 10px 20px;
  background-color: #ccc;
  display: flex;
  justify-content: space-between;
  align-items: center;

  img{
    filter: invert();
    max-width: 160px;
    height: auto;
    flex: 1;
  }

  >div{
    flex: 1;
    text-align: right;
  }
`
  
export function Header() {
  return (
    <CustomHeader>
      <img
        src="https://santex.wpengine.com/wp-content/uploads/2019/02/logo-santex@3x.png"
        alt="logo"
      />
      <div>$ 0</div>
    </CustomHeader>
  );
}
